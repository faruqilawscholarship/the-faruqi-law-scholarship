Faruqi & Faruqi, LLP is a national law firm dedicated to securing the future of the law industry internationally. As such, we are happy to announce our first ever scholarship program for first year law students in the United States.

Applications for the 2019 scholarship program are now being accepted.

Website: http://www.faruqilawscholarships.com
